package au.com.julianoprado.priceRule.models.discount

interface Discount{
    val id: Long;
    val itemSku: String?;
    val discount: Number?;
}