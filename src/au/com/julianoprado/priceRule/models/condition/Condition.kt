package au.com.julianoprado.priceRule.models.condition

interface Condition{
    val id: Long;
    val itemSku: String;
    val condition: Number
}