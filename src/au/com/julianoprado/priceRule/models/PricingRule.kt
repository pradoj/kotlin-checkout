package au.com.julianoprado.priceRule.models

import au.com.julianoprado.priceRule.models.condition.Condition
import au.com.julianoprado.priceRule.models.discount.Discount

data class PricingRule(val conditions: List<Condition>, val discounts: List<Discount>) {

}