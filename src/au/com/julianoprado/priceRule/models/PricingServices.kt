package au.com.julianoprado.priceRule.models

import au.com.julianoprado.IConditionTypeService
import au.com.julianoprado.IDiscountTypeService
import au.com.julianoprado.priceRule.models.condition.Condition
import au.com.julianoprado.priceRule.models.discount.Discount
import kotlin.reflect.KClass

class PricingServices (val discountServices: List<Pair<KClass<out Discount>, IDiscountTypeService<out Discount>>>? = null,
                       val conditionServices: List<Pair<KClass<out Condition>, IConditionTypeService<out Condition>>>? = null){
}