package au.com.julianoprado

import au.com.julianoprado.checkout.models.CheckoutItem
import au.com.julianoprado.priceRule.models.condition.Condition

interface IConditionTypeService<T: Condition> {
    fun getInstance(): IConditionTypeService<T>;

    fun matchConditions(checkoutItems: List<CheckoutItem>,  condition: Condition): Int
}