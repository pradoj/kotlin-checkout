package au.com.julianoprado.priceRule.services.condition

import au.com.julianoprado.IConditionTypeService
import au.com.julianoprado.checkout.models.CheckoutItem
import au.com.julianoprado.priceRule.models.condition.Condition
import kotlin.reflect.KClass

class ConditionService private constructor() {
    object Holder { val INSTANCE = ConditionService() }

    companion object {
        val INSTANCE: ConditionService by lazy { Holder.INSTANCE }
    }


    private var services = mutableMapOf<KClass<out Condition>, IConditionTypeService<out Condition>>()

    fun matchConditions(
            checkoutItems: List<CheckoutItem>, condition: Condition): Int {
        val service = this.services.get(condition::class);
        if (service != null) {
            return service.getInstance().matchConditions(checkoutItems, condition)

        } else {
            throw IllegalArgumentException("Discount not found");
        }
    }

    fun <T: Condition> addService(conditionClass: KClass<out T>, service: IConditionTypeService<out T>){
        this.services.set(conditionClass, service )
    }
}


