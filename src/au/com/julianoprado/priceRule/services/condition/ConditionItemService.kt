package au.com.julianoprado.checkout.services

import au.com.julianoprado.IConditionTypeService
import au.com.julianoprado.checkout.models.CheckoutItem
import au.com.julianoprado.priceRule.models.condition.Condition
import au.com.julianoprado.priceRule.models.condition.ConditionItem

class ConditionItemService private constructor(): IConditionTypeService<ConditionItem> {
    override fun getInstance(): IConditionTypeService<ConditionItem> {
        return instance
    }

    object Holder { val INSTANCE = ConditionItemService() }

    companion object {
        val instance: ConditionItemService by lazy { Holder.INSTANCE }
    }

    override fun matchConditions(checkoutItems: List<CheckoutItem>, condition: Condition): Int {
        if (condition !is ConditionItem) {
            return 0
        }
        if (condition.condition == 0) return 0
        var items = checkoutItems.filter { it.item.sku == condition.itemSku }
        if (items.size == 0) return 0;
        return items.size.div(condition.condition)

    }

}