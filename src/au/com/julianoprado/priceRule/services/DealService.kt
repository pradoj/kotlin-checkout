package au.com.julianoprado.priceRule.services

import au.com.julianoprado.checkout.models.CheckoutItem
import au.com.julianoprado.checkout.services.ConditionItemService
import au.com.julianoprado.priceRule.models.PricingRule
import au.com.julianoprado.priceRule.models.condition.Condition
import au.com.julianoprado.priceRule.models.condition.ConditionItem
import au.com.julianoprado.priceRule.models.discount.DiscountItem
import au.com.julianoprado.priceRule.models.discount.DiscountPrice
import au.com.julianoprado.priceRule.services.condition.ConditionService
import au.com.julianoprado.priceRule.services.discount.DiscountItemService
import au.com.julianoprado.priceRule.services.discount.DiscountPriceService
import au.com.julianoprado.priceRule.services.discount.DiscountService


class DealService private constructor(){
        object Holder { val INSTANCE = DealService() }

        companion object {
            val instance: DealService by lazy { Holder.INSTANCE }
        }

        init {
            ConditionService.INSTANCE.addService(ConditionItem::class, ConditionItemService.instance)
            DiscountService.instance.addService(DiscountItem::class, DiscountItemService.instance)
            DiscountService.instance.addService(DiscountPrice::class, DiscountPriceService.instance)
        }

        fun applyPricingRule(pricingRule: PricingRule, checkoutItems: List<CheckoutItem>): List<CheckoutItem>{
            val discountQuantityApplicable = checkConditions(pricingRule.conditions, checkoutItems);
            return applyDiscounts(pricingRule, checkoutItems, discountQuantityApplicable)
        }

        private fun applyDiscounts(pricingRule: PricingRule, checkoutItems: List<CheckoutItem>, discountQuantityApplicable: Int): List<CheckoutItem> {
            var checkoutItemWithDiscounts = checkoutItems

            pricingRule.discounts.forEach{
                checkoutItemWithDiscounts = DiscountService.instance.applyDiscount(checkoutItems, discountQuantityApplicable, it);
            }
            return checkoutItemWithDiscounts;
        }

        private fun checkConditions(
                conditions: List<Condition>,
                items: List<CheckoutItem>): Int{
            var lowestQuantityApplicable = 0;
            var quantityApplicable: Int;
            for (condition in conditions){
                quantityApplicable = ConditionService.INSTANCE.matchConditions(items, condition);
                if (lowestQuantityApplicable == 0 || lowestQuantityApplicable > quantityApplicable) {
                    lowestQuantityApplicable = quantityApplicable
                }
                if (lowestQuantityApplicable == 0){
                    return lowestQuantityApplicable;
                }
            }
            return lowestQuantityApplicable;
        }
    }