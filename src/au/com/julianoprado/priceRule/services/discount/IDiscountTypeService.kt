package au.com.julianoprado

import au.com.julianoprado.checkout.models.CheckoutItem
import au.com.julianoprado.priceRule.models.discount.Discount

interface IDiscountTypeService<T: Discount> {
    fun getInstance(): IDiscountTypeService<T>;

    fun applyDiscount(checkoutItems: List<CheckoutItem>, discountQuantityApplicable: Int, discount: Discount): List<CheckoutItem>

}