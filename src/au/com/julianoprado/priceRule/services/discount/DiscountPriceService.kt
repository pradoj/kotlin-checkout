package au.com.julianoprado.priceRule.services.discount

import au.com.julianoprado.IDiscountTypeService
import au.com.julianoprado.checkout.models.CheckoutItem
import au.com.julianoprado.priceRule.models.discount.Discount
import au.com.julianoprado.priceRule.models.discount.DiscountPrice

class DiscountPriceService  private constructor(): IDiscountTypeService<DiscountPrice> {
    override fun getInstance(): IDiscountTypeService<DiscountPrice> {
        return instance;
    }

    override fun applyDiscount(checkoutItems: List<CheckoutItem>, discountQuantityApplicable: Int, discount: Discount): List<CheckoutItem> {
        if (discount is DiscountPrice){
        for (checkoutItem in checkoutItems){
            if (checkoutItem.item.sku == discount.itemSku){
                if (checkoutItem.value > discount.discount) {
                    checkoutItem.discountIds.add(discount.id);
                    checkoutItem.value = discount.discount;
                }
            }
        }}

        return checkoutItems;
    }

    object Holder { val INSTANCE = DiscountPriceService() }

    companion object {
        val instance: DiscountPriceService by lazy { Holder.INSTANCE }
    }
   }