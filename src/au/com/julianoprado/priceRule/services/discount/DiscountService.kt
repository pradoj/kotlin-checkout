package au.com.julianoprado.priceRule.services.discount

import au.com.julianoprado.IDiscountTypeService
import au.com.julianoprado.checkout.models.CheckoutItem
import au.com.julianoprado.priceRule.models.discount.Discount
import kotlin.reflect.KClass

class DiscountService private constructor() {
    object Holder { val instance = DiscountService() }

    companion object {
        val instance: DiscountService by lazy { Holder.instance }
    }

    protected var services = mutableMapOf<KClass<out Discount>, IDiscountTypeService<out Discount>>()

    fun applyDiscount(
            checkoutItems: List<CheckoutItem>, discountQuantityApplicable: Int, discount: Discount): List<CheckoutItem> {
        val service = this.services.get(discount::class);
        if (service != null) {
            return service.getInstance().applyDiscount(checkoutItems, discountQuantityApplicable, discount)

        } else {
            throw IllegalArgumentException("Discount not found");
        }
    }

    fun <T: Discount> addService(discount: KClass<out T>, service: IDiscountTypeService<out T>){
        this.services.set(discount, service )
    }
}