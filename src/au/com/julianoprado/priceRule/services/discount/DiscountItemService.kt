package au.com.julianoprado.priceRule.services.discount

import au.com.julianoprado.IDiscountTypeService
import au.com.julianoprado.checkout.models.CheckoutItem
import au.com.julianoprado.priceRule.models.discount.Discount
import au.com.julianoprado.priceRule.models.discount.DiscountItem

class DiscountItemService(): IDiscountTypeService<DiscountItem> {
    override fun getInstance(): IDiscountTypeService<DiscountItem> {
        return instance;
    }

    object Holder { val INSTANCE = DiscountItemService() }

    companion object {
        val instance: DiscountItemService by lazy { Holder.INSTANCE }
    }

    override fun applyDiscount(checkoutItems: List<CheckoutItem>, discountQuantityApplicable: Int, discount: Discount): List<CheckoutItem> {
        if (discount is DiscountItem) {
            for (checkoutItem in checkoutItems) {
                var dealAmount = discount.discount
                if (checkoutItem.item.sku.equals(discount.itemSku) && !checkoutItem.discountIds.contains(discount.id)) {
                    if (checkoutItem.value > 0) {
                        if (dealAmount-- >= 0) {
                            checkoutItem.discountIds.add(discount.id);
                            checkoutItem.value = 0.0;
                        }
                        if (dealAmount == 0) {
                            return checkoutItems
                        }
                    }
                }
            }
        }
        return checkoutItems;
    }

}