package au.com.julianoprado.checkout.models

class CheckoutItem (val item: Item, var discountIds: MutableList<Long> = mutableListOf<Long>()){
    var value = item.value;
}