package au.com.julianoprado.checkout.component

import au.com.julianoprado.checkout.models.CheckoutItem
import au.com.julianoprado.checkout.models.Item
import au.com.julianoprado.priceRule.models.PricingRule
import au.com.julianoprado.priceRule.models.PricingServices
import au.com.julianoprado.priceRule.services.DealService
import au.com.julianoprado.priceRule.services.condition.ConditionService
import au.com.julianoprado.priceRule.services.discount.DiscountService

class Checkout(
        val pricingRules: List<PricingRule> = listOf<PricingRule>(),
        val pricingServices: PricingServices = PricingServices()
) {

    init {
        addServices(pricingServices);
    }
    var checkoutItems: MutableList<CheckoutItem> = mutableListOf<CheckoutItem>();

    fun scan (item: Item){
        this.checkoutItems.add(CheckoutItem(item));
    }

    fun total(): Double {
        this.applyPriceRules()
        var total = 0.0;
        for (checkoutItem in checkoutItems){
            total += checkoutItem.value;
        }
        return total;
    };

    fun applyPriceRules(){
        var updatedCheckoutItems = this.checkoutItems.toList();
        for (pricingRule in this.pricingRules)
            updatedCheckoutItems = DealService.instance.applyPricingRule(pricingRule, checkoutItems)
        this.checkoutItems = updatedCheckoutItems.toMutableList();
    }

    private fun addServices(pricingServices: PricingServices){
        pricingServices.also{
            if ( it.discountServices != null ){
                it.discountServices.forEach{
                    DiscountService.instance.addService(it.first, it.second);
                }
            }
            if ( it.conditionServices != null ){
                it.conditionServices.forEach{
                    ConditionService.INSTANCE.addService(it.first, it.second);
                }
            }
        }
    }

}