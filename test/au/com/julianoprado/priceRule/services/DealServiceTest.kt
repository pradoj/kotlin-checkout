package au.com.julianoprado.priceRule.services

import au.com.julianoprado.checkout.models.CheckoutItem
import au.com.julianoprado.checkout.models.Item
import au.com.julianoprado.priceRule.models.PricingRule
import au.com.julianoprado.priceRule.models.condition.ConditionItem
import au.com.julianoprado.priceRule.models.discount.DiscountItem
import au.com.julianoprado.priceRule.models.discount.DiscountPrice
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test


internal class DealServiceTest {

    @Test
    fun applyPriceRuleDiscountItem() {

        val priceRule = PricingRule(listOf(ConditionItem(0, "0", 1)), listOf(DiscountItem(0, "0", 1)))
        var checkoutItems = listOf(CheckoutItem(Item("0", "t1", 100.0)), CheckoutItem(Item ("0", "t1", 100.0)))

        checkoutItems = DealService.instance.applyPricingRule(priceRule, checkoutItems)
        Assertions.assertEquals(0.0, checkoutItems[0].value)
    }

    @Test
    fun applyPriceRuleDiscountPrice() {
        val priceRule = PricingRule(listOf(ConditionItem(0, "0", 2)), listOf(DiscountPrice(0, "1", 10.0)))
        var checkoutItems = listOf (CheckoutItem(Item("0", "t0", 100.0)), CheckoutItem(Item ("1", "t1", 200.0)))

        checkoutItems = DealService.instance.applyPricingRule(priceRule, checkoutItems)
        Assertions.assertEquals(2, checkoutItems.size)
        Assertions.assertEquals(100.0, checkoutItems[0].value)
    }

}