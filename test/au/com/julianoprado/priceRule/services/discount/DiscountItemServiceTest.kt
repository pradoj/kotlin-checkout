package au.com.julianoprado.priceRule.services.discount

import au.com.julianoprado.checkout.models.CheckoutItem
import au.com.julianoprado.checkout.models.Item
import au.com.julianoprado.priceRule.models.discount.DiscountItem
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class DiscountItemServiceTest {

    @Test
    fun applyDiscount() {

        val checkoutItems = mutableListOf(CheckoutItem(Item("0", "t1", 100.0)), CheckoutItem(Item ("0", "t1", 100.0)))

        val checkoutItemsDiscounted = DiscountItemService.instance.applyDiscount(checkoutItems, 1, DiscountItem(0, "0", 1));
        var value = 0.0
        checkoutItemsDiscounted.forEach{
            value += it.value
        }

        Assertions.assertEquals(100.0, value)


    }

    @Test
    fun applyNoDiscount() {
        val checkoutItems = mutableListOf(CheckoutItem(Item("0", "t1", 100.0)), CheckoutItem(Item ("0", "t1", 100.0)))

        val checkoutItemsDiscounted = DiscountItemService.instance.applyDiscount(checkoutItems, 1, DiscountItem(0, "0", 0));
        var value = 0.0
        checkoutItemsDiscounted.forEach{
            value += it.value
        }

        Assertions.assertEquals(0.0, value)


    }
}