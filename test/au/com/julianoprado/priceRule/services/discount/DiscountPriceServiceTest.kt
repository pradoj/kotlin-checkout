package au.com.julianoprado.priceRule.services.discount

import au.com.julianoprado.checkout.models.CheckoutItem
import au.com.julianoprado.checkout.models.Item
import au.com.julianoprado.priceRule.models.discount.DiscountPrice
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class DiscountPriceServiceTest {

    @Test
    fun applyDiscount() {

        val checkoutItems = mutableListOf(CheckoutItem(Item("0", "t1", 100.0)), CheckoutItem(Item ("0", "t1", 100.0)))

        val checkoutItemsDiscounted = DiscountPriceService.instance.applyDiscount(checkoutItems, 3, DiscountPrice(0, "0", 50.00));

        var value = 0.0
        checkoutItemsDiscounted.forEach{
            value += it.value
        }

        Assertions.assertEquals(100.0, value)


    }

    @Test
    fun applyNoDiscount() {
        val checkoutItems = mutableListOf(CheckoutItem(Item("0", "t1", 100.0)), CheckoutItem(Item ("0", "t1", 100.0)))

        val checkoutItemsDiscounted = DiscountPriceService.instance.applyDiscount(checkoutItems, 0, DiscountPrice(0, "1", 50.00));
        var value = 0.0
        checkoutItemsDiscounted.forEach{
            value += it.value
        }

        Assertions.assertEquals(200.0, value)


    }
}