package au.com.julianoprado.priceRule.services.discount

import au.com.julianoprado.IDiscountTypeService
import au.com.julianoprado.checkout.component.MockItems
import au.com.julianoprado.checkout.models.CheckoutItem
import au.com.julianoprado.priceRule.models.discount.Discount

class DiscountTest(override val id: Long, override val itemSku: String? = null, override val discount: Number? = null, val extra: Boolean) : Discount {

}

class ServiceTest: IDiscountTypeService<DiscountTest> {

    override fun getInstance(): IDiscountTypeService<DiscountTest> {
        return ServiceTest()
    }

    override fun applyDiscount(checkoutItems: List<CheckoutItem>, discountQuantityApplicable: Int, discount: Discount): List<CheckoutItem> {
        if (discount is DiscountTest) {
            if (discount.extra) {
                return listOf<CheckoutItem>(CheckoutItem(MockItems().vga))
            }
        }
        return listOf<CheckoutItem>()
    }

}