package au.com.julianoprado.priceRule.services.discount

import au.com.julianoprado.checkout.component.MockItems
import au.com.julianoprado.checkout.models.CheckoutItem
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

internal class DiscountServiceTest {
    var mockItems = MockItems();
    @Test
    fun applyDiscount() {
    }

    @Test
    fun addServiceNewService() {


        var checkoutItems = listOf(CheckoutItem(mockItems.appleTv), CheckoutItem(mockItems.appleTv))
        val discount = DiscountTest(0, mockItems.appleTv.sku,1, true)
        assertFailsWith(IllegalArgumentException::class){
            DiscountService.instance.applyDiscount(checkoutItems, 1, discount)
        };

        DiscountService.instance.addService(DiscountTest::class, ServiceTest());

        checkoutItems = DiscountService.instance.applyDiscount(checkoutItems, 1, discount);

        assertEquals(mockItems.vga.sku, checkoutItems[0].item.sku)

    }

}