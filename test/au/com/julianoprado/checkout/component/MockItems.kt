package au.com.julianoprado.checkout.component

import au.com.julianoprado.checkout.models.Item

class MockItems {
    val superIpad = Item("ipd", "Super iPad", 549.99)
    val macBookPro = Item("mbp", "MacBook Pro", 1399.99)
    val appleTv = Item("atv", "Apple TV", 109.50)
    val vga = Item("vga", "VGA adaptor", 30.00)
}