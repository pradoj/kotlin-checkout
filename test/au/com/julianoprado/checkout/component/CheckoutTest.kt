package au.com.julianoprado.checkout.component

import au.com.julianoprado.IDiscountTypeService
import au.com.julianoprado.priceRule.models.PricingRule
import au.com.julianoprado.priceRule.models.PricingServices
import au.com.julianoprado.priceRule.models.condition.ConditionItem
import au.com.julianoprado.priceRule.models.discount.Discount
import au.com.julianoprado.priceRule.models.discount.DiscountItem
import au.com.julianoprado.priceRule.models.discount.DiscountPrice
import au.com.julianoprado.priceRule.services.discount.DiscountTest
import au.com.julianoprado.priceRule.services.discount.ServiceTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class CheckoutTest {
    val mockItems = MockItems();

    @Test
    fun scan() {
        val checkout = Checkout();
        checkout.scan(mockItems.superIpad)
        assertTrue(checkout.checkoutItems::isNotEmpty)
    }

    @Test
    fun totalWithoutDiscount() {
        val checkout = Checkout();
        checkout.scan(mockItems.superIpad)
        checkout.scan(mockItems.superIpad)
        assertEquals(1099.98, checkout.total(), 0.1)
    }

    @Test
    fun totalBuy3pay2() {
        val priceRule = PricingRule(
                listOf(ConditionItem(0, "atv", 3)),
                listOf(DiscountItem(0, "atv", 1))
                )


        val checkout = Checkout(listOf(priceRule));

        checkout.scan(mockItems.appleTv)
        checkout.scan(mockItems.appleTv)
        checkout.scan(mockItems.appleTv)
        checkout.scan(mockItems.vga)

        assertEquals(
                249.00,
                checkout.total(),
                0.1
        )
    }

    @Test
    fun totalDiscountBulkMoreThen4() {
        val priceRule = PricingRule(
                listOf(ConditionItem(0, mockItems.superIpad.sku, 5)),
                listOf(DiscountPrice(1, mockItems.superIpad.sku, 499.99))
        )
        
        val checkout = Checkout(listOf(priceRule));
        checkout.scan(mockItems.appleTv)
        checkout.scan(mockItems.superIpad)
        checkout.scan(mockItems.superIpad)
        checkout.scan(mockItems.appleTv)
        checkout.scan(mockItems.superIpad)
        checkout.scan(mockItems.superIpad)
        checkout.scan(mockItems.superIpad)

        assertEquals(2718.95, checkout.total(), 0.1)
    }

    @Test
    fun totalDiscountBundle() {
        val priceRule = PricingRule(
                listOf(ConditionItem(0, mockItems.macBookPro.sku, 1)),
                listOf(DiscountItem(1, mockItems.vga.sku, 1))
        )
        val checkout = Checkout(listOf(priceRule));
        checkout.scan(mockItems.macBookPro)
        checkout.scan(mockItems.vga)
        checkout.scan(mockItems.superIpad)

        assertEquals(1949.98, checkout.total(), 0.1)
    }

    @Test
    fun addNewDiscountBehaviour() {
        val priceRule = PricingRule(
                listOf(ConditionItem(0, "atv", 1)),
                listOf(DiscountTest(0, extra = true))
        )
        val pricingServices = PricingServices(
                listOf(Pair(DiscountTest::class, ServiceTest() as IDiscountTypeService<Discount>)))

        val checkout = Checkout(listOf(priceRule), pricingServices);

        checkout.scan(mockItems.appleTv)
        checkout.scan(mockItems.appleTv)
        checkout.scan(mockItems.appleTv)
        checkout.scan(mockItems.vga)

        assertEquals(
                mockItems.vga.value,
                checkout.total(),
                0.1
        )
    }
}